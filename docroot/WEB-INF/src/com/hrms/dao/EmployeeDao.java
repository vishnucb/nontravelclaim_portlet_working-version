package com.hrms.dao;

import java.util.List;

import org.hibernate.HibernateException;

import com.hrms.entity.Employee;

public interface EmployeeDao {

	

	public List<Employee> getEmployeeList() throws HibernateException;
	public boolean saveEmployee(Employee employee)throws HibernateException;
	public boolean updateEmployee(Employee employee)throws HibernateException;
	public Employee getLogin(String userName,String password)throws HibernateException;
	public boolean deleteEmployee(Employee employee)throws HibernateException;
	public Employee getEmployeeById(long id)throws HibernateException;
	public List<Employee> getEmployeeByName(String name)throws HibernateException;
	public List<Employee> getEmployeeByEmailId(String emailId)throws HibernateException;
	public String getEmployeePassword(long empId) throws HibernateException;
	
	
	
	
	
	
}
