package com.hrms.service;

import java.util.List;

import com.hrms.entity.Employee;

public interface EmployeeService {


	
	List<Employee> getEmployeeList();
	void saveEmployee(Employee employee);
	boolean updateEmployee(Employee employee);
	void deleteEmployee(Employee employee);
	Employee getEmployeeById(long id);
	Employee getEmployeeByName(String name);
	Employee getEmployeeByEmailId(String emailId);
	Employee getLogin(String userName,String password);

	
}
