package com.hrms.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.hrms.dao.EmployeeDao;
import com.hrms.entity.Employee;
import com.hrms.service.EmployeeService;

public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	EmployeeDao employeeDao;
	
	
	@Override
	public List<Employee> getEmployeeList() {
		// TODO Auto-generated method stub
		return employeeDao.getEmployeeList();
	}

	@Override
	public void saveEmployee(Employee employee) {
		// TODO Auto-generated method stub
		employeeDao.saveEmployee(employee);
	}

	@Override
	public boolean updateEmployee(Employee employee) {
		// TODO Auto-generated method stub
		return employeeDao.updateEmployee(employee); 
	}

	@Override
	public void deleteEmployee(Employee employee) {
		// TODO Auto-generated method stub
		employeeDao.deleteEmployee(employee);
	}

		// TODO Auto-generated method stub
		

/*	@Override
	public Employee getEmployeeByName(String name) {
		return employeeDao.getEmployeeByName(name);
	}

	@Override
	public Employee getEmployeeByEmailId(String emailId) {
		// TODO Auto-generated method stub
		return employeeDao.getEmployeeByEmailId(emailId);
	}*/

	public EmployeeDao getEmployeeDao() {
		return employeeDao;
	}

	public void setEmployeeDao(EmployeeDao employeeDao) {
		this.employeeDao = employeeDao;
	}

	@Override
	public Employee getEmployeeById(long id) {
		// TODO Auto-generated method stub
		return employeeDao.getEmployeeById(id);
	}

	@Override
	public Employee getLogin(String userName, String password) {
		// TODO Auto-generated method stub
		return employeeDao.getLogin(userName, password);
	}

	@Override
	public Employee getEmployeeByName(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Employee getEmployeeByEmailId(String emailId) {
		// TODO Auto-generated method stub
		return null;
	}

	
	
}
