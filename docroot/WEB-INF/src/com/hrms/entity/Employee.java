/**
 * 
 */
package com.hrms.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

//@Component
//@Scope("session")

@Entity
@Table(name="EMPLOYEE")
public class Employee implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8347884508618771643L;
	
	@Id
	@Column(name="EMP_ID")
	@GeneratedValue	
	private long empId;
	
	@Column(name="FIRST_NAME", nullable=true)
	private String fName;
	
	@Column(name="MIDDLE_NAME", nullable=true)
	private String mName;
	
	@Column(name="LAST_NAME", nullable=true)
	private String lName;
	
	@Column(name="PASSWORD", nullable=true)
	private String password;
	
	@Column(name="GENDER", nullable=true)
	private String gender;
	
	@Column(name="DATE_OF_BIRTH", nullable=true)
	private Date  dateOfBirth;
	
	@Column(name="DATE_OF_JOINING", nullable=true)
	private Date dateOfJoining;
	
	@Column(name="DESIGNATION", nullable=true)
	private String designation;
	
	@Column(name="OFFICE_EMIAL_ID", nullable=true)
	private String officeEmailId;
	
	@Column(name="PERSONAL_EMAIL_ID", nullable=true)
	private String personalEmailId;
	
	@Column(name="ALTERNATE_EMAIL_ID", nullable=true)
	private String alternateEmailId;
	
	@Column(name="TOTAL_EXPERIENCE", nullable=true)
	private String totalExperience;
	
	@Column(name="MOBILE_N0", nullable=true)
	private long mobileNo;
	
	@Column(name="OFFICE_NO", nullable=true)
	private long officeNo;
	
	@Column(name="HOME_NO", nullable=true)
	private long homeNo;
	
	@Column(name="EMERGENCY_NO", nullable=true)
	private long emergencyNo;
	
	@Column(name="PAN_NO", nullable=true)
	private String panNo;
	
	@Column(name="UAN_NO", nullable=true)
	private String uanNo;
	
	@Column(name="PF_NO", nullable=true)
	private String pfNo;
	
	@Column(name="NOTICE_PERIOD", nullable=true)
	private long noticePeriod;
	
	@Column(name="ADMIN", nullable=true)
	private boolean admin;

	public Employee() {
		super();
		// TODO Auto-generated constructor stub
	}

	public long getEmpId() {
		return empId;
	}

	public void setEmpId(long empId) {
		this.empId = empId;
	}

	public String getfName() {
		return fName;
	}

	public void setfName(String fName) {
		this.fName = fName;
	}

	public String getmName() {
		return mName;
	}

	public void setmName(String mName) {
		this.mName = mName;
	}

	public String getlName() {
		return lName;
	}

	public void setlName(String lName) {
		this.lName = lName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public Date getDateOfJoining() {
		return dateOfJoining;
	}

	public void setDateOfJoining(Date dateOfJoining) {
		this.dateOfJoining = dateOfJoining;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getOfficeEmailId() {
		return officeEmailId;
	}

	public void setOfficeEmailId(String officeEmailId) {
		this.officeEmailId = officeEmailId;
	}

	public String getPersonalEmailId() {
		return personalEmailId;
	}

	public void setPersonalEmailId(String personalEmailId) {
		this.personalEmailId = personalEmailId;
	}

	public String getAlternateEmailId() {
		return alternateEmailId;
	}

	public void setAlternateEmailId(String alternateEmailId) {
		this.alternateEmailId = alternateEmailId;
	}

	public String getTotalExperience() {
		return totalExperience;
	}

	public void setTotalExperience(String totalExperience) {
		this.totalExperience = totalExperience;
	}

	public long getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(long mobileNo) {
		this.mobileNo = mobileNo;
	}

	public long getOfficeNo() {
		return officeNo;
	}

	public void setOfficeNo(long officeNo) {
		this.officeNo = officeNo;
	}

	public long getHomeNo() {
		return homeNo;
	}

	public void setHomeNo(long homeNo) {
		this.homeNo = homeNo;
	}

	public long getEmergencyNo() {
		return emergencyNo;
	}

	public void setEmergencyNo(long emergencyNo) {
		this.emergencyNo = emergencyNo;
	}

	public String getPanNo() {
		return panNo;
	}

	public void setPanNo(String panNo) {
		this.panNo = panNo;
	}

	public String getUanNo() {
		return uanNo;
	}

	public void setUanNo(String uanNo) {
		this.uanNo = uanNo;
	}

	public String getPfNo() {
		return pfNo;
	}

	public void setPfNo(String pfNo) {
		this.pfNo = pfNo;
	}

	public long getNoticePeriod() {
		return noticePeriod;
	}

	public void setNoticePeriod(long noticePeriod) {
		this.noticePeriod = noticePeriod;
	}

	public boolean isAdmin() {
		return admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "Employee [empId=" + empId + ", fName=" + fName + ", mName="
				+ mName + ", lName=" + lName + ", password=" + password
				+ ", gender=" + gender + ", dateOfBirth=" + dateOfBirth
				+ ", dateOfJoining=" + dateOfJoining + ", designation="
				+ designation + ", officeEmailId=" + officeEmailId
				+ ", personalEmailId=" + personalEmailId
				+ ", alternateEmailId=" + alternateEmailId
				+ ", totalExperience=" + totalExperience + ", mobileNo="
				+ mobileNo + ", officeNo=" + officeNo + ", homeNo=" + homeNo
				+ ", emergencyNo=" + emergencyNo + ", panNo=" + panNo
				+ ", uanNo=" + uanNo + ", pfNo=" + pfNo + ", noticePeriod="
				+ noticePeriod + ", admin=" + admin + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (admin ? 1231 : 1237);
		result = prime
				* result
				+ ((alternateEmailId == null) ? 0 : alternateEmailId.hashCode());
		result = prime * result
				+ ((dateOfBirth == null) ? 0 : dateOfBirth.hashCode());
		result = prime * result
				+ ((dateOfJoining == null) ? 0 : dateOfJoining.hashCode());
		result = prime * result
				+ ((designation == null) ? 0 : designation.hashCode());
		result = prime * result + (int) (emergencyNo ^ (emergencyNo >>> 32));
		result = prime * result + (int) (empId ^ (empId >>> 32));
		result = prime * result + ((fName == null) ? 0 : fName.hashCode());
		result = prime * result + ((gender == null) ? 0 : gender.hashCode());
		result = prime * result + (int) (homeNo ^ (homeNo >>> 32));
		result = prime * result + ((lName == null) ? 0 : lName.hashCode());
		result = prime * result + ((mName == null) ? 0 : mName.hashCode());
		result = prime * result + (int) (mobileNo ^ (mobileNo >>> 32));
		result = prime * result + (int) (noticePeriod ^ (noticePeriod >>> 32));
		result = prime * result
				+ ((officeEmailId == null) ? 0 : officeEmailId.hashCode());
		result = prime * result + (int) (officeNo ^ (officeNo >>> 32));
		result = prime * result + ((panNo == null) ? 0 : panNo.hashCode());
		result = prime * result
				+ ((password == null) ? 0 : password.hashCode());
		result = prime * result
				+ ((personalEmailId == null) ? 0 : personalEmailId.hashCode());
		result = prime * result + ((pfNo == null) ? 0 : pfNo.hashCode());
		result = prime * result
				+ ((totalExperience == null) ? 0 : totalExperience.hashCode());
		result = prime * result + ((uanNo == null) ? 0 : uanNo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employee other = (Employee) obj;
		if (admin != other.admin)
			return false;
		if (alternateEmailId == null) {
			if (other.alternateEmailId != null)
				return false;
		} else if (!alternateEmailId.equals(other.alternateEmailId))
			return false;
		if (dateOfBirth == null) {
			if (other.dateOfBirth != null)
				return false;
		} else if (!dateOfBirth.equals(other.dateOfBirth))
			return false;
		if (dateOfJoining == null) {
			if (other.dateOfJoining != null)
				return false;
		} else if (!dateOfJoining.equals(other.dateOfJoining))
			return false;
		if (designation == null) {
			if (other.designation != null)
				return false;
		} else if (!designation.equals(other.designation))
			return false;
		if (emergencyNo != other.emergencyNo)
			return false;
		if (empId != other.empId)
			return false;
		if (fName == null) {
			if (other.fName != null)
				return false;
		} else if (!fName.equals(other.fName))
			return false;
		if (gender == null) {
			if (other.gender != null)
				return false;
		} else if (!gender.equals(other.gender))
			return false;
		if (homeNo != other.homeNo)
			return false;
		if (lName == null) {
			if (other.lName != null)
				return false;
		} else if (!lName.equals(other.lName))
			return false;
		if (mName == null) {
			if (other.mName != null)
				return false;
		} else if (!mName.equals(other.mName))
			return false;
		if (mobileNo != other.mobileNo)
			return false;
		if (noticePeriod != other.noticePeriod)
			return false;
		if (officeEmailId == null) {
			if (other.officeEmailId != null)
				return false;
		} else if (!officeEmailId.equals(other.officeEmailId))
			return false;
		if (officeNo != other.officeNo)
			return false;
		if (panNo == null) {
			if (other.panNo != null)
				return false;
		} else if (!panNo.equals(other.panNo))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (personalEmailId == null) {
			if (other.personalEmailId != null)
				return false;
		} else if (!personalEmailId.equals(other.personalEmailId))
			return false;
		if (pfNo == null) {
			if (other.pfNo != null)
				return false;
		} else if (!pfNo.equals(other.pfNo))
			return false;
		if (totalExperience == null) {
			if (other.totalExperience != null)
				return false;
		} else if (!totalExperience.equals(other.totalExperience))
			return false;
		if (uanNo == null) {
			if (other.uanNo != null)
				return false;
		} else if (!uanNo.equals(other.uanNo))
			return false;
		return true;
	}
	
//	@OneToMany(mappedBy = "employee", cascade = CascadeType.ALL)
//	private Set<NonTravelClaim> nontravelclaim;
	
	
	

	
	
}
