package com.hrms.entity;


import java.io.Serializable;
import java.sql.Blob;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="NONTRAVELCLAIM")
public class NonTravelClaim implements Serializable{

	private static final long serialVersionUID = 6699904149017744731L;

	/**
	 * 
	 */

	
	@Id
	@Column(name="TRANSACTION_ID")
	@GeneratedValue	
	private int transactionID;
	
	@Column(name="TYPE_OF_CLAIM")
	private String typeOfClaim;
	
	@Column(name="DATE_OF_CLAIM")
	private Date dateForClaim;
	
	@Column(name="CLAIM_AMOUNT")
	private double claimAmount;
	
	@Column(name="CERTIFICATION_TYPE")
	private String certificationType;
	
	@Column(name="CERTIFICATION_NAME")
	private String certificationName;
	
	@Column(name="DESCRIPTION")
	private String description;
	
	@Column(name="CONTENT")
	@Lob
	private Blob content;
	
	@ManyToOne
	@JoinColumn(name = "EMP_ID", nullable=false)
	private Employee employee;
	
	
	public Blob getContent() {
		return content;
	}
	public void setContent(Blob content) {
		this.content = content;
	}
	/*
	@OneToOne
	@JoinColumn(name = "FILE_ID", nullable=true)
	private UploadFile uploadfile;
	*/
	public String getTypeOfClaim() {
		return typeOfClaim;
	}
	public Date getDateForClaim() {
		return dateForClaim;
	}
	public double getClaimAmount() {
		return claimAmount;
	}
	public String getCertificationType() {
		return certificationType;
	}
	public String getCertificationName() {
		return certificationName;
	}
	public String getDescription() {
		return description;
	}
	public void setTypeOfClaim(String typeOfClaim) {
		this.typeOfClaim = typeOfClaim;
	}
	public void setDateForClaim(Date dateForClaim) {
		this.dateForClaim = dateForClaim;
	}
	public void setClaimAmount(double claimAmount) {
		this.claimAmount = claimAmount;
	}
	public void setCertificationType(String certificationType) {
		this.certificationType = certificationType;
	}
	public void setCertificationName(String certificationName) {
		this.certificationName = certificationName;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((certificationName == null) ? 0 : certificationName.hashCode());
		result = prime * result + ((certificationType == null) ? 0 : certificationType.hashCode());
		long temp;
		temp = Double.doubleToLongBits(claimAmount);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((dateForClaim == null) ? 0 : dateForClaim.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((typeOfClaim == null) ? 0 : typeOfClaim.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NonTravelClaim other = (NonTravelClaim) obj;
		if (certificationName == null) {
			if (other.certificationName != null)
				return false;
		} else if (!certificationName.equals(other.certificationName))
			return false;
		if (certificationType == null) {
			if (other.certificationType != null)
				return false;
		} else if (!certificationType.equals(other.certificationType))
			return false;
		if (Double.doubleToLongBits(claimAmount) != Double.doubleToLongBits(other.claimAmount))
			return false;
		if (dateForClaim == null) {
			if (other.dateForClaim != null)
				return false;
		} else if (!dateForClaim.equals(other.dateForClaim))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (typeOfClaim == null) {
			if (other.typeOfClaim != null)
				return false;
		} else if (!typeOfClaim.equals(other.typeOfClaim))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "NonTravelClaim [typeOfClaim=" + typeOfClaim + ", dateForClaim=" + dateForClaim + ", claimAmount="
				+ claimAmount + ", certificationType=" + certificationType + ", certificationName=" + certificationName
				+ ", description=" + description + "]";
	}
	public int getTransactionID() {
		return transactionID;
	}
	public void setTransactionID(int transactionID) {
		this.transactionID = transactionID;
	}
	public Employee getEmployee() {
		return employee;
	}
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
/*	public UploadFile getUploadfile() {
		return uploadfile;
	}
	public void setUploadfile(UploadFile uploadfile) {
		this.uploadfile = uploadfile;
	}
	
	*/
}
