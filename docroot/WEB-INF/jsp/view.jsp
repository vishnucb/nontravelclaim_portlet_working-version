<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<portlet:defineObjects />

<portlet:renderURL var="TravelrenderURL">
	<portlet:param name="action" value="travelShowForm"/> <!-- showForm -->
</portlet:renderURL>

<portlet:renderURL var="NonTravelrenderURL">
	<portlet:param name="action" value="nonTravelShowForm"/> <!-- showForm -->
</portlet:renderURL>

<portlet:renderURL var="uploadRenderURL">
	<portlet:param name="action" value="upload"/> <!-- showForm -->
</portlet:renderURL>

<h3>Claim Portlet</h3>

<br/><br/>

<a href="${TravelrenderURL}">Go to Travel Claim</a>
<br/><br/>
<a href="${NonTravelrenderURL}">Go to Non-Travel Claim</a>
<br/><br/>
<a href="${uploadRenderURL}">Go to UploadFile</a>



