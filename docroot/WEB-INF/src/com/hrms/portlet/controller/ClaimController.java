
package com.hrms.portlet.controller;

import com.hrms.dao.EmployeeDao;
import com.hrms.dao.EmployeeDaoImpl;
import com.hrms.entity.Employee;
import com.hrms.entity.NonTravelClaim;
import com.hrms.entity.TravelClaim;
import com.hrms.service.NonTravelClaimService;
import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.dao.jdbc.OutputBlob;
//import com.hrms.service.NonTravelClaimService;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;

import com.hrms.dao.FileUploadDAO;
import com.hrms.entity.UploadFile;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import java.io.File;

import org.apache.commons.io.FileUtils;

import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

@Controller("claimController") //
@MultipartConfig
@RequestMapping("VIEW")
public class ClaimController {
    private Log log = LogFactoryUtil.getLog((String)ClaimController.class.getName());

   // @Autowired
  //  private ContactService contactservice;
    @Autowired
    private NonTravelClaimService nontravelclaimservice;
    @Autowired
    private EmployeeDao employeedao;
    @Autowired
	private FileUploadDAO fileUploadDao;
    
    
private final static int ONE_GB = 1073741824;
	
	private final static String baseDir = "D:/Liferay/Test";
	
	private final static String fileInputName = "fileupload";
    
    @InitBinder
	  public void initBinder(WebDataBinder dataBinder)
	  {
	   SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd"); //mm/dd/yyyy
	   format.setLenient(false);
	   dataBinder.registerCustomEditor(Date.class, new CustomDateEditor(format, true));
	  }
    
	@RenderMapping
    public String viewHomePage(RenderRequest request, RenderResponse response) {
        this.log.info((Object)"#################Calling viewHomePage############");
        return "view";
    }
	
	 @RenderMapping(params={"action=travelShowForm"})
	    public String showTravelForm(Map<String, Object> map) {
	        this.log.info((Object)"##############Calling showTravelForm###########");
	        TravelClaim tc = new TravelClaim();
	        map.put("travelClaim", (Object)tc);
	        return "travelClaimForm";
	    }


    @RenderMapping(params={"action=nonTravelShowForm"})
    public String showNonTravelForm(Map<String, Object> map) {
        this.log.info((Object)"##############Calling showNonTravelForm###########");
        NonTravelClaim ntc = new NonTravelClaim();
        map.put("nontravelclaim", (Object)ntc);
        ArrayList<String> typeOfClaimList = new ArrayList<String>();
        typeOfClaimList.add("Certification");
        typeOfClaimList.add("Food reimbursment");
        typeOfClaimList.add("Cab");
        typeOfClaimList.add("Other");
        map.put("typeOfClaimList", typeOfClaimList);
        return "form";
    }
    
   
    @RenderMapping(params={"action=Certification"})
    public String viewSuccess1() {
        this.log.info((Object)"#############Calling Certification###########");
        return "expensesClaimCertification";
    }

    @RenderMapping(params={"action=Food reimbursment"})
    public String viewSuccess2() {
        this.log.info((Object)"#############Calling Food reimbursment###########");
        return "expensesClaimFood";
    }

    @RenderMapping(params={"action=Cab"})
    public String viewSuccess3() {
        this.log.info((Object)"#############Calling Cab###########");
        return "expensesClaimCab";
    }

    @RenderMapping(params={"action=Other"})
    public String viewSuccess4() {
        this.log.info((Object)"#############Calling Other###########");
        return "expensesClaimOther";
    }
    
    @RenderMapping(params={"action=success"})
    public String viewSuccess5() {
        this.log.info((Object)"#############Calling viewSuccess###########");
        return "success";
    }
    
    @RenderMapping(params={"action=failed"})
    public String viewSuccess6() {
        this.log.info((Object)"#############Calling failed###########");
        return "failed";
    }
    
    @RenderMapping(params={"action=upload"})
    public String viewSuccess7(Map<String, Object> map) {
        this.log.info((Object)"#############Calling Upload jsp###########");
      
        NonTravelClaim ntc = new NonTravelClaim();
        map.put("nontravelclaim", (Object)ntc);
        ArrayList<String> typeOfClaimList = new ArrayList<String>();
        typeOfClaimList.add("Certification");
        typeOfClaimList.add("Food reimbursment");
        typeOfClaimList.add("Cab");
        typeOfClaimList.add("Other");
        map.put("add", typeOfClaimList);
        return "EmployeeAddress";
       // return "upload";
    }

    @ActionMapping(value="handleCustomer")
    public void getCustomerData(@ModelAttribute(value="nontravelclaim") NonTravelClaim nontravelclaim, 
    		BindingResult result, ActionRequest actionRequest, ActionResponse actionResponse, Model model) {
        actionResponse.setRenderParameter("action", nontravelclaim.getTypeOfClaim());
        model.addAttribute("successModel", (Object)nontravelclaim);
        if (nontravelclaim.getTypeOfClaim().equals("Certification")) {
            ArrayList<String> certificationTypeList = new ArrayList<String>();
            ArrayList<String> certificationNameList = new ArrayList<String>();
            certificationTypeList.add("Java");
            certificationTypeList.add("Loma");
            certificationTypeList.add("ABAP");
            model.addAttribute("certificationTypeList", certificationTypeList);
            certificationNameList.add("LOMA 280");
            certificationNameList.add("Java 8");
            certificationNameList.add("ABAP");
            model.addAttribute("certificationNameList", certificationNameList);
        }
    }

    @ActionMapping(value="handleSaveData")
    public void saveNonTravelClaimData(@ModelAttribute(value="nontravelclaim") NonTravelClaim nontravelclaim, 
    	//	@RequestParam("file") MultipartFile file,
    		BindingResult result, ActionRequest actionRequest, ActionResponse actionResponse, Model model)
    				throws Exception {
    	
    	this.log.info((Object)"#############Calling handleSaveData###########");
    	
    	UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(actionRequest);
    	
    	String startDateString = uploadRequest.getParameter("dateForClaim");    	
    	 DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
    	 Date startDate = df.parse(startDateString);

    	Employee emp =	employeedao.getEmployeeById(3L);
    	nontravelclaim.setEmployee(emp);

    	nontravelclaim.setTypeOfClaim(uploadRequest.getParameter("typeOfClaim"));
    	nontravelclaim.setCertificationName(uploadRequest.getParameter("certificationName"));//"certificationNameList"));
    	nontravelclaim.setCertificationType(uploadRequest.getParameter("certificationType"));//"certificationTypeList"));
    	nontravelclaim.setClaimAmount(Double.parseDouble(uploadRequest.getParameter("claimAmount")));
    	nontravelclaim.setDateForClaim(startDate);
    	nontravelclaim.setDescription(uploadRequest.getParameter("description"));
    	

    	File file = uploadRequest.getFile("file", true); // file is the input name from my jsp
    	FileInputStream fileInput = new FileInputStream(file);
    	OutputBlob blobOutput = new OutputBlob(fileInput, file.length());
     	nontravelclaim.setContent(blobOutput);
    
    	try{
    	
    		nontravelclaimservice.saveNonTravelClaim(nontravelclaim);
    		actionResponse.setRenderParameter("action", "success");
    		}
    		catch(Exception e)
    		{
    			actionResponse.setRenderParameter("action", "failed");
    			
    		}  
    }
    
    @ActionMapping(value="doUpload")
  //  @RenderMapping(params={"action=doUpload"})
    public void saveNonTravelClaimData(ActionRequest request, ActionResponse response)
			throws Exception {
         
    	UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(request);
    	 
		long sizeInBytes = uploadRequest.getSize(fileInputName);
 
		if (uploadRequest.getSize(fileInputName) == 0) {
			throw new Exception("Received file is 0 bytes!");
		}
 
		// Get the uploaded file as a file.
		File uploadedFile = uploadRequest.getFile(fileInputName);
 
		String sourceFileName = uploadRequest.getFileName(fileInputName);
 
		
		// Where should we store this file?
		File folder = new File(baseDir);
 
		// Check minimum 1GB storage space to save new files...
		
		if (folder.getUsableSpace() < ONE_GB) {
			throw new Exception("Out of disk space!");
		}
 
		// This is our final file path.
		File filePath = new File(folder.getAbsolutePath() + File.separator + sourceFileName);
 
		// Move the existing temporary file to new location.
		FileUtils.copyFile(uploadedFile, filePath);
    }
}
