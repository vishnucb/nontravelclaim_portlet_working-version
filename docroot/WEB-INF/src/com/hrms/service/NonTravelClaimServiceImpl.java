package com.hrms.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hrms.dao.EmployeeDao;
import com.hrms.dao.NonTravelClaimDao;
import com.hrms.entity.NonTravelClaim;

@Service
public class NonTravelClaimServiceImpl implements NonTravelClaimService{
	
	@Autowired
	private NonTravelClaimDao nontravelclaimdao; // = new NonTravelClaimDaoImpl();
	@Autowired
	private EmployeeDao employeedao;
	
	@Transactional
	public void saveNonTravelClaim(NonTravelClaim nontravelclaim) {
				
	//	employeedao.getEmployeeById(id)
		nontravelclaimdao.saveNonTravelClaim(nontravelclaim);
	}

	@Transactional
	public void updateNonTravelClaim(NonTravelClaim nontravelclaim) {
		// TODO Auto-generated method stub
		
	}

	@Transactional
	public NonTravelClaim getNonTravelClaim(NonTravelClaim nontravelclaim) {
		// TODO Auto-generated method stub
		return null;
	}

}
