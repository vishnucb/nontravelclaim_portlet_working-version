package com.hrms.dao;

import com.hrms.entity.UploadFile;

public interface FileUploadDAO {
	void save(UploadFile uploadFile);
}
