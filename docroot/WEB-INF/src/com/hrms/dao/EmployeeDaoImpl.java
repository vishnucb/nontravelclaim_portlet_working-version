package com.hrms.dao;

import java.util.ArrayList;
import java.util.List;

//<<<<<<< HEAD
//=======




//>>>>>>> branch 'master' of https://Ashwanipandey@bitbucket.org/tulsirock/hrms_dao.git
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hrms.entity.Employee;

@Repository
public class EmployeeDaoImpl implements EmployeeDao {

	@Autowired
	private SessionFactory factory;

	@Override  //SaveEmployee working, tested
	public boolean saveEmployee(Employee employee) {
	
		return true;
	}

	@Override  //tested working
	public boolean updateEmployee(Employee employee) {
	
		return true;
	}

	@Override //delete employee tested working
	public boolean deleteEmployee(Employee employee) {
		
		
		return true;
	}

	@Override //tested working
	public Employee getEmployeeById(long id) {
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		Employee employee;
		try {
		//	id = 3L;
			employee= (Employee) session.get(Employee.class, id);
			System.out.println("THE EMPLOYEE DETAILS ARE:" + employee);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				System.out.println("Error while fetching employee details, Rollback in process....");
				tx.rollback();
				System.out.println("Error while fetching employee details, Rollback transaction done");
				e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
		return employee;
	}

	@Override  //tested working
	public List<Employee> getEmployeeByName(String name) {
	
		List<Employee> list= new ArrayList<Employee>();

		return list;
	}

	@Override  //tested working
	public List<Employee> getEmployeeByEmailId(String emailId) {
		
		List<Employee> list= new ArrayList<Employee>();
		
		return list;

	}


	@Override
	public Employee getLogin(String userName, String password) {
		
		return null;
	}


	@Override
	public String getEmployeePassword(long empId) throws HibernateException {
		
	
		return null;

	}


	@Override  //tested working
	public List<Employee> getEmployeeList() throws HibernateException {
		// TODO Auto-generated method stub
	
		return null;
	}


}
