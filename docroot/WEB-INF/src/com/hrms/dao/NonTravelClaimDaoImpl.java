package com.hrms.dao;


import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;


import org.springframework.stereotype.Repository;

import com.hrms.entity.Employee;
import com.hrms.entity.NonTravelClaim;

@Repository
public class NonTravelClaimDaoImpl implements NonTravelClaimDao{

//	@Autowired
//	private SessionFactory factory=new Configuration().configure().buildSessionFactory();
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public boolean saveNonTravelClaim(NonTravelClaim nontravelclaim){
	
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		try{
			
		//	Employee emp = (Employee) session.get(Employee.class, 3L);
		//	nontravelclaim.setEmployee(emp);
			session.save(nontravelclaim);
			tx.commit();
			return true;
		}
		catch(Exception e)
		{
			System.out.println(e);
			return false;
		
		} finally {
		session.close();
		}
	}

	@Override
	public boolean updateNonTravelClaim(NonTravelClaim nontravelclaim)
			throws HibernateException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public NonTravelClaim getNonTravelClaim(NonTravelClaim nontravelclaim)
			throws HibernateException {
		// TODO Auto-generated method stub
		return null;
	}

}
