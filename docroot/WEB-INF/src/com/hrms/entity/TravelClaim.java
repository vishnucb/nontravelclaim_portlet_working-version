package com.hrms.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;


/**
 * 
 * @author Shreedath G N 
 *
 */
public class TravelClaim implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String empName;
	private String employeeId;
	private String fromDate;
	private String toDate;
	private String countryTraveled;
	private String projectTraveled;
	private String expenseType;
	private String receiptAmount;
	private String receiptCurrency;
	private String perDiemCollectedinput;
	private String advanceAvailedinput;
	private String balanceToBeRepaidinput;
	private String claimType;
	private String baseCurrency;
	
	
	
	/*private List<ExpenseDetailsModel> expenseDetailsModel;// = new ArrayList<ExpenseDetailsModel>();
	
	
	public List<ExpenseDetailsModel> getExpenseDetailsModel() {
		return expenseDetailsModel;
	}
	public void setExpenseDetailsModel(List<ExpenseDetailsModel> expenseDetailsModel) {
		this.expenseDetailsModel = expenseDetailsModel;
	}*/
	public String getClaimType() {
		return claimType;
	}
	public void setClaimType(String claimType) {
		this.claimType = claimType;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	private String approver;
	
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public String getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public String getCountryTraveled() {
		return countryTraveled;
	}
	public void setCountryTraveled(String countryTraveled) {
		this.countryTraveled = countryTraveled;
	}
	public String getApprover() {
		return approver;
	}
	public void setApprover(String approver) {
		this.approver = approver;
	}
	public String getProjectTraveled() {
		return projectTraveled;
	}
	public void setProjectTraveled(String projectTraveled) {
		this.projectTraveled = projectTraveled;
	}
	public String getExpenseType() {
		return expenseType;
	}
	public void setExpenseType(String expenseType) {
		this.expenseType = expenseType;
	}
	public String getReceiptAmount() {
		return receiptAmount;
	}
	public void setReceiptAmount(String receiptAmount) {
		this.receiptAmount = receiptAmount;
	}
	public String getReceiptCurrency() {
		return receiptCurrency;
	}
	public void setReceiptCurrency(String receiptCurrency) {
		this.receiptCurrency = receiptCurrency;
	}
	public String getPerDiemCollectedinput() {
		return perDiemCollectedinput;
	}
	public void setPerDiemCollectedinput(String perDiemCollectedinput) {
		this.perDiemCollectedinput = perDiemCollectedinput;
	}
	public String getAdvanceAvailedinput() {
		return advanceAvailedinput;
	}
	public void setAdvanceAvailedinput(String advanceAvailedinput) {
		this.advanceAvailedinput = advanceAvailedinput;
	}
	public String getBalanceToBeRepaidinput() {
		return balanceToBeRepaidinput;
	}
	public void setBalanceToBeRepaidinput(String balanceToBeRepaidinput) {
		this.balanceToBeRepaidinput = balanceToBeRepaidinput;
	}
	public String getBaseCurrency() {
		return baseCurrency;
	}
	public void setBaseCurrency(String baseCurrency) {
		this.baseCurrency = baseCurrency;
	}
	
}
