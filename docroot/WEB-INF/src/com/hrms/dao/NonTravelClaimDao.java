package com.hrms.dao;

import org.hibernate.HibernateException;

import com.hrms.entity.NonTravelClaim;


public interface NonTravelClaimDao {
	
	
	public boolean saveNonTravelClaim(NonTravelClaim nontravelclaim)throws HibernateException;
	public boolean updateNonTravelClaim(NonTravelClaim nontravelclaim)throws HibernateException;
	
	public NonTravelClaim getNonTravelClaim(NonTravelClaim nontravelclaim)throws HibernateException;
	
}
