<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
    <%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
        <portlet:defineObjects />
        <portlet:actionURL var="submitTravelClaimFormURL" name="submit" />

        <head>

        </head>

        <body>
            <form:form name="travelClaim" id="travelClaim" method="post" modelAttribute="travelClaim" action="<%=submitTravelClaimFormURL.toString() %>">
                <section>
                    <div>
                        <h3 class="emp-heading">Expense Claim </h3>

                        <!--<form:form name="travelClaim" method="post" modelAttribute="travelClaim" action="<%=submitTravelClaimFormURL.toString() %>"> -->

                        <br/>
                        
                       <div class="form-group" style="float:left;padding-right:30px;">
  									<label class="sr-only"  for="username" style="width:180px;clear:left;text-align:right;padding-right:30px;">First Name</label>
  									<input id="user_first_name" name="user[first_name]" size="30" type="text" style="padding-right:30px;" />
					 </div>

					   <div class="form-group" style="float:left; padding-right:30px;">
  								<label class="sr-only" for="name" style="width:180px;clear:left;text-align:right;padding-right:30px;">Last Name</label>
  								<input id="user_last_name" name="user[last_name]" size="30" type="text" style="padding-right:30px;"/>
						</div>
						
						<br/>
                        <table>
                            <tbody>
                                <tr>
                                    <td>
                                        <form:label path="claimType">Nature of Claim:</form:label>
                                    </td>
                                    <td>
                                        <form:select path="claimType" multiple="false" style="padding-right:30px;">
                                            <option value="" disabled="disabled" selected="selected">Select Claim</option>
                                            <option value="1">Travel</option>
                                            <option value="2">Non Travel</option>

                                        </form:select>
                                    </td>
                               <!--  </tr>
                                <tr> -->
                               		 <td>
                                
                               		 </td>
                                    <td>
                                        <form:label path="employeeId">Employee Id:</form:label>
                                    </td>
                                    <td>
                                        <form:input type="text" path="employeeId"></form:input>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <form:label path="empName">Employee Name:</form:label>
                                    </td>
                                    <td>
                                        <form:input type="text" path="empName"></form:input>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <form:label path="approver">Approver:</form:label>
                                    </td>
                                    <td>
                                        <form:select path="approver" multiple="false">
                                            <option value="" disabled="disabled" selected="selected">Select Approver</option>
                                            <option value="1">Project Manager</option>
                                            <option value="2">Country Manager</option>

                                        </form:select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <form:label path="fromDate">From Date:</form:label>
                                    </td>
                                    <td>
                                        <form:input type="date" path="fromDate"></form:input>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <form:label path="toDate">To Date</form:label>
                                    </td>
                                    <td>
                                        <form:input type="date" path="toDate"></form:input>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <form:label path="countryTraveled">Country Traveled:</form:label>
                                    </td>
                                    <td>
                                        <form:input type="text" path="countryTraveled"></form:input>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <form:label path="projectTraveled">Project Traveled:</form:label>
                                    </td>
                                    <td>
                                        <form:input type="text" path="projectTraveled"></form:input>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <form:label path="baseCurrency">Base Currency:</form:label>
                                    </td>
                                    <td>
                                        <form:input type="text" path="baseCurrency"></form:input>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <%-- <div>
				     <label class="control-label col-sm-4" for="employee name">
					     Employee Name :
					 </label>
					<div class="col-sm-4">
					<form:input type="text" name = "employeeName" path ="empName" class="form-control" placeholder = "employee name" value="${employee.empName}" readonly="true"/> 
					</div>
				 </div>
				 <div class="form-group">
				     <label class="control-label col-sm-4" for="emp Id">
					     Employee ID :
					 </label>
					<div class="col-sm-4">
					<form:input type="text" name = "employeeId" path="empID" class="form-control" placeholder = "employee ID" value="${employee.empId}" readonly="true"/> 
					</div>
				 </div>
				 <div class="form-group">
				    <label class="control-label col-sm-4" for="Meal Preference ">
					    Approver :
					 </label>
					 <div class="col-sm-4">
					 	<form:select class="form-control" path="approver"  multiple="false">   
					 	 <option value="" disabled="disabled" selected="selected">Select Approver</option>
   							<option value="1">Project Manager</option>
    						<option value="2">Country Manager</option>

						</form:select>
					</div>
				 </div>

               <div class="form-group">
				     <label class="control-label col-sm-4" for="select from date">
					   From Date :
					 </label>
					 <div class="col-sm-4">
					      <form:input type="date" path ="fromDate" class="form-control" placeholder="travel from date"/>
					 </div>
				 </div>
				 <div class="form-group">
				     <label class="control-label col-sm-4" for="select from date">
					   To Date :
					 </label>
					 <div class="col-sm-4">
					      <form:input type="date" path ="toDate" class="form-control" placeholder="travel to date"/>
					 </div>
				 </div>

				 <div class="form-group">
				     <label class="control-label col-sm-4" for="country travelled">
					     Country Travelled :
					 </label>
					<div class="col-sm-4">
					<form:input type="text" path ="countryTravelled" name = "countryName" class="form-control" placeholder="country travelled"/> 
					</div>
				 </div>
				 <div class="form-group">
				     <label class="control-label col-sm-4" for="project travelled">
					     Project Travelled for :
					 </label>
					<div class="col-sm-4">
					<form:input type="text" path = "projectTravelled" name = "project name" class="form-control" placeholder="project travelled for" /> 
					</div>
				 </div>
				 <div class="form-group">
				     <label class="control-label col-sm-4" for="expense type">
					     Expense Type :
					 </label>
					<div class="col-sm-4">
					<form:select id="ExpenseType" class="form-control" path="expenseType"  multiple="false" onclick="showTextBox();">
        									<form:option value="" selected="selected" label="--Please Select"/>
        									<form:options items="${expenseType}"  />
					</form:select>
					</div>
						 </div>
					<div id = "receiptAmount" class="form-group" style="visibility:hidden">
				     <label class="control-label col-sm-4" for="Receipt/Amount">
					     Receipt/Amount:
					 </label>
					<div class="col-sm-2">
					<input type="text" name = "<portlet:namespace />Receipt/Amount" class="form-control" placeholder="Amount" /> 
					</div>
					<label class="control-label col-sm-2" for="Submit Bill" style= "margin-left: -60px;">
					     Submit Bill:
					 </label>
					<div class="col-sm-4">
					<input type="text" name = "<portlet:namespace />Receipt Currency" class="form-control" placeholder="Receipt Currency" /> 
					</div>
					<button type="button" class="btn btn-default">Add</button>
				 </div>
				 	 <div class="form-group">
				     <label class="control-label col-sm-4" for="perdiem type">
					     Per Diem for travel :
					 </label>
					<div class="col-sm-4">
					<form:input type="text" name ="perDiemCollectedinput" path ="perDiemCollected" class="form-control" placeholder="perdiem collected" /> 
					</div>
				 </div>
				 	 <div class="form-group">
				     <label class="control-label col-sm-4" for="advance type">
					     Advance Availed :
					 </label>
					<div class="col-sm-4">
					<form:input type="text" name ="advanceAvailedinput" class="form-control" path="advanceAvailed" placeholder="advance availed for" /> 
					</div>
				 </div>
				 	 <div class="form-group">
				     <label class="control-label col-sm-4" for="balance type">
					     Balance to be repaid :
					 </label>
					<div class="col-sm-4">
					<form:input type="text" name = "balanceToBeRepaidinput" class="form-control" path="balanceToBeRepaid" placeholder="Balance to be repaid"/> 
					</div>
				 </div>--%>

                            <div class="col-sm-8  pull-right">
                                <!--<button class="btn btn-info btn-color" type="submit">Submit</button>-->
                            </div>
                            <!-- </form:form> -->
                    </div>

                </section>
                <section>
                    <h3 class="emp-heading">Expense details </h3>
                    <div>

                        <!--<form class="form-horizontal" role="form">-->
                        <!-- <div class="plus-div">
							       <span id="anc_add"><i class="fa fa-plus-circle"></i></span>
							   </div>
							    <div class="minus-div">
							       <span id="anc_rem"><i class="fa fa-minus-circle"></i></span>
							   </div>-->
                        <div class="buttin-alignment-wrap">
                            <span id="anc_add" class="btn btn-default" onclick="myFunction()"><i class="fa fa-plus-circle" onclick="myFunction()"></i>ADD Row</span>
                            <span id="anc_rem" class="btn btn-default" onclick="deleteRow('tbl1')"><i class="fa fa-minus-circle"></i>DEL Row</span>
                        </div>

                        <div class="table-responsive">
                            <div style="width:auto;overflow:auto;height:auto;">
                                <table class="table table-bordered" id="tbl1">
                                    <thead>
                                        <tr>
                                            <th>
                                                <input class='check_all' type="checkbox" id="checkAll" name="checkbox" />Select All</th>

                                            <th>
                                                Sl No.
                                            </th>
                                            <th>
                                                Receipt / Bill Amount
                                            </th>

                                            <th>
                                                Receipt / Bill Currency
                                            </th>
                                            <th>
                                                Exchange Rate to Base Curreny
                                            </th>
                                            <th>
                                                Exchange to Base Currency
                                            </th>
                                            <th>
                                                Expense Type
                                            </th>
                                            <th>
                                                Project Code (If on Project, else Cost Center Code)
                                            </th>
                                            <th>
                                                Task Code
                                            </th>
                                            <th>
                                                Missing Receipt
                                            </th>
                                            <th>
                                                Expense Group
                                            </th>
                                            <th>
                                                Justification
                                            </th>
                                            <th>
                                                Additional Information
                                            </th>
                                            <th>
                                                Upload Document
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="target1">
                                            <td>
                                                <input type="checkbox" id="checkbox" class="case" />
                                            </td>
                                            <td>1.</td>
                                            <td>
                                                <input type="text" id="text1" style="border:'5px';border-color:'thick black'; " placeholder="">
                                            </td>
                                            <td>
                                                <input type="text" id="text2" style="border:'5px';border-color:'thick black'; " placeholder="" />
                                            </td>
                                            <td>
                                                <input type="text" id="text3" style="border:'5px';border-color:'thick black'; " placeholder="" />
                                            </td>
                                            <td>
                                                <input type="text" id="text4" style="border:'5px';border-color:'thick black'; " placeholder="" />
                                            </td>
                                            <td>
                                                <!--<label for="sel1">Select list:</label>-->
                                                <select class="form-control" id="selection" name="<portlet:namespace />selection">
                                                    <option value="0">--Select Expense Type--</option>
                                                    <option value="opt1">Airfare - Billable</option>
                                                    <option value="opt2">Lodging - Billable</option>
                                                    <option value="opt3">Meals - Billable</option>
                                                    <option value="opt4">Auto - Billable</option>
                                                    <option value="opt4">Other - Non Billable</option>
                                                    <option value="opt4">Telephone - Billable</option>

                                                </select>
                                            </td>
                                            <td>
                                                <input type="text" id="text5" style="border:'5px';border-color:'thick black'; " placeholder="" />
                                            </td>
                                            <td>
                                                <input type="text" id="text6" style="border:'5px';border-color:'thick black';" placeholder="" />
                                            </td>
                                            <td>
                                                <input type="text" id="text7" style="border:'5px';border-color:'thick black';" placeholder="" />
                                            </td>
                                            <td>
                                                <select class="form-control" id="selection2" name="<portlet:namespace />selection">
                                                    <option value="0">--Select Expense Group--</option>
                                                    <option value="opt1">Perdiem</option>
                                                    <option value="opt2">Lodging</option>
                                                    <option value="opt3">Travel</option>
                                                    <option value="opt4">Food</option>
                                                    <option value="opt4">Fx Pos Purchase</option>
                                                    <option value="opt4">Markup Fee Service Tax</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input type="text" id="text8" style="border:'5px';border-color:'thick black';" placeholder="" />
                                            </td>
                                            <td>
                                                <input type="text" id="text9" style="border:'5px';border-color:'thick black';" placeholder="" />
                                            </td>
                                            <!--  <td>
								       <select class="form-control" id="specialization" name="<portlet:namespace />selection">
											<option  value="0">--Select specialization--</option>
											<option  value="opt1">CSE</option>
											<option value="opt2">Electronics</option>
											<option value="opt3">BSC</option>
											<option value="opt4">SSC or Class X Equivalent</option>
									        <option value="opt4">B.com</option>
									        <option value="opt4">M.com</option>
								      </select>
								 </td>
								  <td>
								    <input type="text" style="border:'5px';border-color:'thick black'; " placeholder="percentage" />
								 </td> -->
                                            <td>
                                                <input type="file" id="file" style="border:none;border-color:transparent;" value="upload Documents" />
                                            </td>

                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <br/>

                        <div class="col-sm-8  pull-right">
                            <input id="educationDetailsvalue" name="EducationDetails" type="hidden" value="" />
                            <input type="submit" class="btn btn-info btn-color" value="Submit" style=" margin-left: -15%; margin-top: 1%;"/>

                            <div class="next-btn pull-right">
                                <input type="reset" class="btn btn-info btn-color" value="Clear" />
                            </div>
                        </div>

                        <!--</form>-->
                    </div>

                </section>

                <!-- <footer class="footer homepageFooter">
      <div id="footer">
        <a href="#">homepage</a> | <a href="mailto:denise@mitchinson.net">contact</a> | <a href="http://validator.w3.org/check?uri=referer">html</a> | <a href="http://jigsaw.w3.org/css-validator">css</a> | &copy; 2007 Anyone | 
      </div>
    </footer>  -->

                <script src="js/jquery.min.js"></script>
                <script src="js/bootstrap.min.js"></script>

                <!-- <script>
                     function myFunction() {
                        alert("myFunction");

                        var table = document.getElementById("tbl1");
                        var rowCount = table.rows.length;
                        var row = table.insertRow(rowCount);

                        var colCount = table.rows[0].cells.length;

                        for (var i = 0; i < colCount; i++) {
                            var cell1 = row.insertCell(i);
                            if (i == 1) {
                                cell1.innerHTML = rowCount;
                            } else {
                                cell1.innerHTML = table.rows[1].cells[i].innerHTML;
                            }

                        }

                    } 
                </script> -->
                <script>
                    function deleteRow(tableID) {
                        alert("deleteRow");
                        var table = document.getElementById(tableID).tBodies[0];
                        var rowCount = table.rows.length;

                        // var i=1 to start after header
                        for (var i = 0; i < rowCount; i++) {
                            var row = table.rows[i];
                            // index of td contain checkbox is 8
                            var chkbox = row.cells[0].getElementsByTagName('input')[0];
                            if ('checkbox' == chkbox.type && true == chkbox.checked) {
                                table.deleteRow(i);
                            }
                        }
                    }
                </script>

                <script>
                    $('#checkAll').change(function() {

                        $('tbody tr td input[type="checkbox"]').prop('checked', $(this).prop('checked'));
                    });
                </script>

                <script>
                    $(document).ready(function() {
                        var i = 1;
                        $('#add-row').click(function() {

                            $('#addr' + i).html("<td><input  name='docType" + i + "' type='text' placeholder='docType'  class='form-control input-md'></td><td><input  name='upload document files" + i + "' type='file' placeholder='upload'  class='form-control input-md'></td>");

                            $('#docTable').append('<tr id="addr' + (i + 1) + '"></tr>');
                            i++;
                        });
                        $("#delete_row").click(function() {
                            if (i > 1) {
                                $("#addr" + (i - 1)).html('');
                                i--;
                            }
                        });

                    });

                  i = 2;
                    $("#anc_add").on('click', function() {
                        var data = '<tr class="target1"><td><input type="checkbox" class="case"/></td>';
                        data += '<td>' + i + '</td>';
                        data += "<td><input type='text' id='col1'" + i + "' name='Receipt[]' style='border:'5px';border-color:'thick black';'/></td>";
                        data += "<td><input type='text' id='col2'" + i + "' name='BillCurrency[]' style='border:'5px';border-color:'thick black';'/></td>";
                        data += "<td><input type='text' id='col3'" + i + "' name='Exchange Rate[]' style='border:'5px';border-color:'thick black';'/></td>";
                        data += "<td><input type='text' id='col4'" + i + "' name='BaseCurrency' style='border:'5px';border-color:'thick black';'/></td>";
                        data += '<td> <select class="form-control" id="selection"' + i + ' name="selection">' + '<option  value="0">--Select Expense Type--</option>' + '<option  value="opt1">Airfare - Billable</option>' + '<option value="opt2">Lodging - Billable</option>' + '<option value="opt3">Meals - Billable</option>' + '<option value="opt4">Auto - Billable</option>' + '<option value="opt5">Other - Non Billable</option>' + '<option value="opt6">Telephone - Billable</option></select></td>';
                        data += "<td><input type='text' id='col5'" + i + "' name='pcode[]' style='border:'5px';border-color:'thick black';'/></td>";
                        data += "<td><input type='text' id='col6'" + i + "' name='tcode[]' style='border:'5px';border-color:'thick black';'/></td>";
                        data += "<td><input type='text' id='col7'" + i + "' name='mReceipt[]' style='border:'5px';border-color:'thick black';'/></td>";
                        data += '<td> <select class="form-control" id="selection"' + i + ' name="selection">' + '<option  value="0">--Select Expense Group--</option>' + '<option  value="opt1">Perdiem</option>' + '<option value="opt2">Lodging</option>' + '<option value="opt3">Travel</option>' + '<option value="opt4">Food</option>' + '<option value="opt5">Fx Pos Purchase</option>' + '<option value="opt6">Markup Fee Service Tax</option></select></td>';
                        data += "<td><input type='text' id='col8'" + i + "' name='jfication[]' style='border:'5px';border-color:'thick black';'/></td>";
                        data += "<td><input type='text' id='col9'" + i + "' name='additionalD[]' style='border:'5px';border-color:'thick black';'/></td>";
                        data += "<td><input type='file' id='col10'" + i + "' name='udocument[]' style='border:'5px';border-color:'thick black';'/></td></tr>";
                        $("#tbl1 ").append(data);
                        i++;
                    }); 

                    $("#anc_rem").on('click', function() {
                        $('input[class=case]:checkbox:checked').closest("tr").remove();
                        //$( "#tbl1 tbody tr:last" ).remove();
                    });

                    $("#travelClaim").on('submit', function(e) {

                        //alert('inside');
                        var json = [],
                            obj, j, k, l, rows = $(".target1");
                        //rows = $(e.currentTarget).find('tbody tr');
							//alert('rows.length ' + rows.length);
                        for (var i = 0; i < rows.length; i++) {

                          //  alert('rows.length ' + rows.length);

                            obj = {};
                            j = 0;
                            k = 0;
                            l = 0;
                            //alert("res testing");
                            var res = $(rows[i]).attr('class').match(/target[1]/ig);
                           // alert("res" + res);
                            if ($(rows[i]).attr('class').match(/target[1]/ig)) {
                               // alert('res if as ****' + res);

                                j++;
                                obj.sqNo = $(rows[i]).find('td:eq(' + 1 + ')').text();
                               // alert(" obj.sqNo "+ obj.sqNo);
                                
                                obj.qualification = $(rows[i]).find('select:eq(' + 0 + ')').val();
                               // alert(" obj.qualification "+obj.qualification);
                                
                                obj.college = $(rows[i]).find('input:eq(' + 1 + ')').val();
                               // alert(" obj.college "+ obj.college);
                                
                                obj.board = $(rows[i]).find('input:eq(' + 2 + ')').val();
                               // alert(" obj.board "+  obj.board);
                                
                                obj.startDate = $(rows[i]).find('input:eq(' + 3 + ')').val();
                               // alert(" obj.startDate "+  obj.startDate);
                                
                                obj.endDate = $(rows[i]).find('input:eq(' + 4 + ')').val();
                               // alert(" obj.endDate "+ obj.endDate);
                                //obj.percentage = $(rows[i]).find('input:eq('+5+')').val();

                                //obj.specialization = $(rows[i]).find('td:eq('+7+')').text();
                                //alert('yes');
                                //obj.specialization = $(rows[i]).find('td:eq('+3+')').text();
                                //alert('no');
                                //obj.percentage = $(rows[i]).find('input:eq('+5+')').val();
                                //obj.fileUpload = $(rows[i]).find('input:eq('+6+')').val();
                            } else {
                                alert('res else ' + res);
                            }

                            json.push(obj);
                        }

                        $("#educationDetailsvalue").val(JSON.stringify(json));
                        return true;

                    });

                    /* function select_all(event) {
                    	alert("select_all");
                    	$('input[class=case]:checkbox').each(function(){ 
                    		alert($('input[class=check_all]:checkbox:checked').length == 0);
                    		if($('input[class=check_all]:checkbox:checked').length == 0){ 
                    			$(this).prop("checked", false); 
                    		} else {
                    			$(this).prop("checked", true); 
                    		} 
                    	});
                    }  */
                </script>
            </form:form>
        </body>