<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<portlet:defineObjects />
<portlet:actionURL var="submitFormURL" name="handleSaveData"/>

<form:form method="POST" modelAttribute="nontravelclaim" action="${submitFormURL}" enctype="multipart/form-data">
			
				<div class="form-group">
				<label class="control-label col-sm-4" for="type of claim">Type of Claim :</label>
				<div class="col-sm-4"><form:input class="form-control"  path="typeOfClaim" value="${successModel.typeOfClaim}" readonly="true"></form:input>
				</div>
				</div>
			
			
				<br>
				<div class="form-group">
				<label class="control-label col-sm-4" for="completed date">Date for Claim :</label>
				<div class="col-sm-4"><form:input type="date" class="form-control"  path="dateForClaim"></form:input>
				</div>
				</div>
				<br>
				
				<div class="form-group">
				<label class="control-label col-sm-4" for="certification fees">Amount :</label>
				<div class="col-sm-4">
				<form:input path="claimAmount" class="form-control" placeholder="" ></form:input>
				<label>INR</label>
				</div>
				</div>
				<br>
				
				<div class="form-group">
				<label class="control-label col-sm-4">Document</label>    
				<div class="col-sm-4">
				<input type="file" name="file" ></input>
				</div>
				</div>
				<br>
				
				<div class="col-sm-8  pull-right">
				<button class="btn btn-info btn-color" type="submit">Submit</button>
				</div>
				
				
</form:form>