<%@ page language="java" contentType="text/html; charset=ISO-8859-1"    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<portlet:defineObjects />
<portlet:renderURL var="alternativeViewUrl"><portlet:param name="render" value="alternative-view" /></portlet:renderURL>
<portlet:actionURL var="actionOneUrl"><portlet:param name="action" value="action-one" /></portlet:actionURL>
<portlet:actionURL var="actionTwoUrl"><portlet:param name="action" value="action-two" /></portlet:actionURL>
<portlet:resourceURL var="resourceOneUrl" id="resource-one"><portlet:param name="action" value="action-one" /></portlet:resourceURL>
<portlet:actionURL var="personalInfoUrl"><portlet:param name="action" value="personalInfo-one" /></portlet:actionURL>
<portlet:actionURL var="employeeAddressUrl"><portlet:param name="action" value="employeeAddress" /></portlet:actionURL>
<portlet:actionURL var="educationDetailsUrl"><portlet:param name="action" value="employeeDetails" /></portlet:actionURL>
<portlet:actionURL var="employeeSkillsUrl"><portlet:param name="action" value="skills" /></portlet:actionURL>
<portlet:actionURL var="employmentDetailsUrl"><portlet:param name="action" value="employmentDetails" /></portlet:actionURL>
<portlet:actionURL var="familyDetailsUrl"><portlet:param name="action" value="familyDetails" /></portlet:actionURL>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="/img/logo.png" />
    <!-- iPad retina icon -->
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="/img/apple-touch-icon-precomposed-152.png"/>
    <!-- iPad retina icon (iOS < 7) -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/img/apple-touch-icon-precomposed-144.png"/>
    <!-- iPad non-retina icon -->
    <link rel="apple-touch-icon-precomposed" sizes="76x76" href="/img/apple-touch-icon-precomposed-76.png"/>
    <!-- iPad non-retina icon (iOS < 7) -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/img/apple-touch-icon-precomposed-72.png"/>
    <!-- iPhone 6 Plus icon -->
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="/img/apple-touch-icon-precomposed-180.png"/>
    <!-- iPhone retina icon (iOS < 7) -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/img/apple-touch-icon-precomposed-114.png"/>
    <!-- iPhone non-retina icon (iOS < 7) -->
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="/img/apple-touch-icon-precomposed-57.png"/>
    <link rel="stylesheet" href="/css/patternfly.min.css" />
    <link rel="stylesheet" href="/css/patternfly-additions.min.css" />
    <link rel="stylesheet" href="/css/index.css" />
    <link rel="stylesheet" href="/css/footer.css"/>
    <script src="//code.jquery.com/jquery-2.1.4.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="/js/patternfly.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-route.js"></script>
	<style type="text/css"></style>
<title>Address</title>


<style type="text/css">

 .hide{
            display: none;
        }
        
</style>



</head>
<body>

		<portlet:defineObjects />
		<portlet:actionURL var="getEmployeeDetailsURL" name="saveAddressInfo"/>
			
			<div class="container-fluid">
					<div class="row">
				        <div class="col-sm-9 col-md-10 col-sm-push-3 col-md-push-2">
					          <ol class="breadcrumb">
					          <!--   <li><a href="#">Home</a></li> -->
					           <!--  <li>Tab</li> -->
					          </ol>
					         
					   
					          <ul class="nav nav-tabs" style="margin-left: 50px;">
					          		<li><a href="${personalInfoUrl}" target="iframe1" > Personal Info</a></li>
				              		<li class="active"> <a href="${employeeAddressUrl}" target="iframe1" class="active">Employee Address</a></li>
					           		<li><a href="${employeeSkillsUrl}" target="iframe1">Skills</a></li>
					           		<li><a href="${educationDetailsUrl}" target="iframe1">Education Details</a></li>
					           		<li><a href="${familyDetailsUrl}" target="iframe1">Family Details</a></li>
			           				<li><a href="${employmentDetailsUrl}" target="iframe1">Employment Details</a></li>
					          </ul>
					               <h2 style="margin-left: 10px;">Employee Address </h2>
						</div>
				 
		
					<hr>
				 	<form:form name="add" method="post" modelAttribute="add" action="<%=getEmployeeDetailsURL.toString() %>" class="form-horizontal">
					     <h3 class="emp-heading">Create Employee Address </h3>
            
     
				        <div class="form-group">
					         <label class="control-label col-sm-4" for="empId">
					             Address Type <span style="color:red;font-size:18px;font-weight:bold;">*</span>: 
					         </label>
					      <div class="col-sm-8">
					
						        <select class="form-control"  id="data" >
						        
							         <option  value="0">--Select Address--</option>
							         <option  value="homeaddress">Home</option>
							         <option value="officeAddress">Office</option>
							         <option value="permanentAddress">Permanent</option>
							         <option value="correspondenceAddress">Correspondence</option>
						         
						       </select>
					      </div>
				     </div>
				<div  id="homeaddress"><!-- Home Address -->
				     
						     <div class="form-group tooltip-demo">
							<table class="table table-bordered table-hover cust-tabs-info" id="form">
									<tbody>
										    <tr>
												<td><form:label path="lineone" >Address Line 1:</form:label></td>
												<td><form:input  id="lineOne" path="lineone"  name="lineone" disabled="false"  class="form-control"	data-toggle="tooltip" data-placement="right"	title="Please enter your address."></form:input></td>
											</tr>
											
											<tr>
											<td><form:label path="linetwo">Address Line 2:</form:label></td>
											<td><form:input path="linetwo" name="linetwo" disabled="false" class="form-control"	data-toggle="tooltip" data-placement="right"	title="Please enter your address."></form:input></td>
											
											</tr>
											
											<tr>
											<td><form:label path="linethree">Address Line 3:</form:label></td>
											<td><form:input path="linethree" name="linethree" disabled="false" class="form-control"	data-toggle="tooltip" data-placement="right"	title="Please enter your address."></form:input></td>
											</tr>
											<tr>
											<td><form:label path="city">City:</form:label></td>
											<td><form:input path="city" disabled="false" name="city" class="form-control"	data-toggle="tooltip" data-placement="right"	title="Please enter your city name."></form:input></td>
											</tr>
											
											
											<tr>
											<td><form:label path="state">State:</form:label></td>
											<td><form:input path="state" disabled="false" name="state" class="form-control"	data-toggle="tooltip" data-placement="right"	title="Please enter your state name."></form:input></td>
											</tr>
											
											
											<tr>
											<td><form:label path="pincode">PinCode:</form:label></td>
											<td><form:input path="pincode" name="pincode" disabled="false" class="form-control"	data-toggle="tooltip" data-placement="right"	title="Please enter your pincode."></form:input></td>
											</tr>
											
											<tr>
											<td><label>Country:</label></td>
											<td>
											<%-- <form:input path="country" name="country" disabled="false" class="form-control" 	data-toggle="tooltip" data-placement="right"	title="Please enter your country name.">
											</form:input> --%>
											 <select class="form-control" id="selection" name="selection">
								        			 <option  value="0">--Select countries--</option>
								       				 <option  value="opt1"> country1</option>
								       				 <option value="opt2"> country2</option>
								       				 <option value="opt3"> country3</option>
								        			 <option value="opt4"> country4</option>
								         
						            </select>
											</td>
											
											</tr>
											
								
									</tbody>
							</table>
							</div>
			     
			     </div><!-- Home Address -->
			     

			     <div class="buttin-alignment-wrap">
			                                <span id="anc_add" class="btn btn-default"><i class="fa fa-plus-circle"></i>Save</span>
			                                <!-- <span id="anc_rem" class="btn btn-default"><i class="fa fa-minus-circle"></i>DEL Row</span> -->
			      </div>
			     
		            <!-- <div class="col-sm-8  pull-right">
		                 <button class="btn btn-info btn-color" type="submit">Submit</button>
		            </div>  -->  
            
     				 <div class="table-responsive">
                        <table class="table table-bordered" id="tbl1">

                            <thead>
                                <tr class="targetHr"></tr>

                            </thead>
                            <tbody>
                                <tr class="target1"></tr>
                            </tbody>
                        </table>
                    </div>
     
        
                     
					
					</form:form>
				</div>
			
		    </div>				
	   	
	   	
			 <script src="js/jquery.min.js"></script>
             <script src="js/bootstrap.min.js"></script>
             
      		
      		 <script>
									            $(document).ready(function(){
									            	alert('on ready');
									            	
									            	var datah ='<tr class="targetHr"><th><b>S.No.</b></th><th><b>Address Type</b></th><th><b>Address Line 1</b></th><th><b>Address Line 2</b></th><th><b>Address Line3</b></th>';
													datah +="<th><b>City</b></th><th><b>State</b></th><th><b>Pin Code</b></th><th><b>Country</b></th></tr>";
													$("#tbl1 ").append(datah);
									            	
									            	 $('.tooltip-demo').tooltip({
										                 
										        		  selector: "[data-toggle=tooltip]",
										        		  container: "body"
										
										        	  });
									            	
									            	
									            	 alert('before load');
								                	 
								                	var i = 1;
											        $("#anc_add").on('click', function() {
											            
											        	var addressTypeH =$("#data").val();
											        	
														var address1H =$("#lineOne").val();
														var address2H =$("#linetwo").val();
														var address3H =$("#linethree").val();
														var cityH =$("#city").val();
														var stateH =$("#state").val();
														var pincodeH =$("#pincode").val();
														var countryH =$("#selection option:selected").text(); 
														var data = '<tr class="target1">';
											            data += '<td>' + i + '.</td>';
											           
											            data += "<td><input type='text' id='col2" + i + "' name='addressType' style='border:'5px';border-color:'thick black';' readonly/></td>";

											            data += "<td><input type='text' id='col3" + i + "' name='lineOne' style='border:'5px';border-color:'thick black';' readonly/></td>";
											            data += "<td><input type='text' id='col4" + i + "' name='linetwo' style='border:'5px';border-color:'thick black';' readonly/></td>";
											
											            data += "<td><input type='text' id='col5" + i + "' name='linethree' style='border:'5px';border-color:'thick black';' readonly/></td>";

											            data += "<td><input type='text' id='col6" + i + "' name='city' style='border:'5px';border-color:'thick black';' readonly/></td>";

											            data += "<td><input type='text' id='col7" + i + "' name='state' style='border:'5px';border-color:'thick black';' readonly/></td>";
											            data += "<td><input type='text' id='col8" + i + "' name='pincode' style='border:'5px';border-color:'thick black';' readonly/></td>";
											           // data += '<td> <select class="form-control" id="country"' + i + ' name="selection">' + '<option  value="0">--Select countries--</option>' + '<option  value="opt1">country1</option>' + '<option value="opt2">country2</option>' + '<option value="opt3">country3</option>' + '<option value="opt4">country4</option></select></td>';
											            data += "<td><input type='text' id='col9" + i + "' name='country' style='border:'5px';border-color:'thick black';' readonly/></td>";
											            data += "<td><button  type='button' id='editbtn' style='height:'50px'; width:'50px;'>Edit</button></td></tr>";

											            $("#tbl1 ").append(data);
											            
											            document.getElementById('col2'+i).value=addressTypeH;
											            document.getElementById('col3'+i).value=address1H;
											            document.getElementById('col4'+i).value=address2H;
											            document.getElementById('col5'+i).value=address3H;
											            document.getElementById('col6'+i).value=cityH;
											            document.getElementById('col7'+i).value=stateH;
											            document.getElementById('col8'+i).value=pincodeH;
											            document.getElementById('col9'+i).value=countryH;
											            i++;
											           
											           // $("#form").reset();
											            
											            $("#editbtn").on('click', function() {
												        	
												        	alert('inside edit function');
												            var currentTD = $(this).parents('tr').find('td');
												            alert(' currentTD '+currentTD);
												            alert(' currentTD.size '+currentTD.size);
												            if ($(this).html() == 'Edit') {   
												            	
												            	alert('if');
												                 $.each(currentTD, function () {
												                   // $(this).prop('readonly', false);
												                   
												                        console.log($(this).text());
												                        console.log($(this).val());
												                }); 
												            	/* $("#data").val();
													        	
																$("#lineOne").val();
																$("#linetwo").val();
																$("#linethree").val();
																$("#city").val();
																$("#state").val();
																$("#pincode").val();
																$("#selection option:selected").text();  */
												                
												                
												                
												            } else {
												            	alert('else');
												               /* $.each(currentTD, function () {
												                    $(this).prop('contenteditable', false);
												                }); */
												                
												                
												                
												            }

												            $(this).html($(this).html() == 'Edit' ? 'Save' : 'Edit');
	
												        });
											            
											        });

											        
									        });
   			 </script> 
    
</body>
</html>