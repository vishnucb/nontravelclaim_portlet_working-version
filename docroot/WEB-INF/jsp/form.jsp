<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<portlet:defineObjects />
<portlet:actionURL var="submitFormURL" name="handleCustomer"/>

<form:form method="POST" modelAttribute="nontravelclaim" action="${submitFormURL}">
<div class="form-group">
				     <label class="control-label col-sm-4" for="type of claim">
					    Type of Claim :
					 </label>
					 <div class="col-sm-4">
			 <form:select class="form-control" path="typeOfClaim" onchange="submit()">
					 <form:option value="" disabled="true" selected="selected">Select</form:option>
					 <form:options items="${typeOfClaimList}"></form:options>
					 </form:select>					 	
			
		<!-- 	<form:input path="typeOfClaim"></form:input>
		<input type="submit" value="Submit Form">
		-->		</div>
				</div>	
</form:form>