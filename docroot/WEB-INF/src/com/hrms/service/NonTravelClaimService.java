package com.hrms.service;

import com.hrms.entity.NonTravelClaim;


public interface NonTravelClaimService {

	public void saveNonTravelClaim(NonTravelClaim nontravelclaim);
	public void updateNonTravelClaim(NonTravelClaim nontravelclaim);
	
	public NonTravelClaim getNonTravelClaim(NonTravelClaim nontravelclaim);
	
	
	
}
